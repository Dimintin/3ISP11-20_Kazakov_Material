﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using _3ISP11_20_Kazakov.DB;
using _3ISP11_20_Kazakov.Classes;
using System.Collections.ObjectModel;
using System;
using System.Collections.Generic;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CheckDiscountForPrice()
        {
            List<VW_ProductList> checklist = new List<VW_ProductList>();

            VW_ProductList product = new VW_ProductList();
            product.Price = 1400;
            checklist.Add(product);

            decimal expected = (decimal)0.17;
            decimal result = BlackFriday.getFridayDiscount(checklist[0]);


            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CheckDiscountForOverPrice()
        {
            List<VW_ProductList> checklist = new List<VW_ProductList>();

            VW_ProductList product = new VW_ProductList();
            product.Price = 200000;
            checklist.Add(product);

            decimal expected = (decimal)0.5;
            decimal result = BlackFriday.getFridayDiscount(checklist[0]);


            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CheckDiscountForNegativePrice()
        {
            List<VW_ProductList> checklist = new List<VW_ProductList>();

            VW_ProductList product = new VW_ProductList();
            product.Price = -34;
            checklist.Add(product);

            decimal expected = (decimal)0.15;
            decimal result = BlackFriday.getFridayDiscount(checklist[0]);


            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CheckDiscountForDecimalPrice()
        {
            List<VW_ProductList> checklist = new List<VW_ProductList>();

            VW_ProductList product = new VW_ProductList();
            product.Price = (decimal)3500.5056;
            checklist.Add(product);

            decimal expected = (decimal)0.3;
            decimal result = BlackFriday.getFridayDiscount(checklist[0]);


            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CheckForAnyBlackFridayDate_True()
        {
            DateTime date = new DateTime(11, 11, 11);

            Assert.IsTrue(BlackFriday.isBlackFriday(date));
        }

        [TestMethod]
        public void CheckForAnyBlackFridayDate_False()
        {
            DateTime date = new DateTime(11, 07, 04);

            Assert.IsFalse(BlackFriday.isBlackFriday(date));
        }

        [TestMethod]
        public void CheckPricingSetForPrice()
        {
            List<VW_ProductList> checklist = new List<VW_ProductList>();

            VW_ProductList product = new VW_ProductList();
            product.Price = 1400;
            checklist.Add(product);

            decimal expected = (decimal)1162;
            BlackFriday.SetFridayDiscount(checklist);
            decimal result = checklist[0].Price;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CheckPricingSetForOverPrice()
        {
            List<VW_ProductList> checklist = new List<VW_ProductList>();

            VW_ProductList product = new VW_ProductList();
            product.Price = 140000;
            checklist.Add(product);

            decimal expected = (decimal)70000;
            BlackFriday.SetFridayDiscount(checklist);
            decimal result = checklist[0].Price;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CheckPricingSetForNegativePrice()
        {
            List<VW_ProductList> checklist = new List<VW_ProductList>();

            VW_ProductList product = new VW_ProductList();
            product.Price = -1400;
            checklist.Add(product);

            decimal expected = (decimal)0;
            BlackFriday.SetFridayDiscount(checklist);
            decimal result = checklist[0].Price;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CheckPricingSetForDecimalPrice()
        {
            List<VW_ProductList> checklist = new List<VW_ProductList>();

            VW_ProductList product = new VW_ProductList();
            product.Price = (decimal)1923.32;
            checklist.Add(product);

            decimal expected = (decimal)1596.36;
            BlackFriday.SetFridayDiscount(checklist);
            decimal result = checklist[0].Price;

            Assert.AreEqual(expected, result);
        }
    }
}
