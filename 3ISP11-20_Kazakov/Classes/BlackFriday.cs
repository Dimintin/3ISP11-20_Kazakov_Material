﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3ISP11_20_Kazakov.Classes
{
    public static class BlackFriday
    {
        private static DateTime blackFridayDate = new DateTime(DateTime.Now.Year, 11, 11);
        private static double defaultFridayDiscount = 0.10;
        public static bool isCheckedForDiscount;

        static public decimal getFridayDiscount(DB.VW_ProductList item) 
        {
            if (item.Price > 5000)
            {
                return (decimal)defaultFridayDiscount * 5;
            }
            else if (item.Price > 4000 && item.Price <= 5000)
            {
                return (decimal)defaultFridayDiscount * 4;
            }
            else if (item.Price > 3000 && item.Price <= 4000)
            {
                return (decimal)defaultFridayDiscount * 3;
            }
            else if (item.Price > 2000 && item.Price <= 3000)
            {
                return (decimal)defaultFridayDiscount * 2;
            }
            else if (item.Price > 1000 && item.Price <= 2000)
            {
                return (decimal)defaultFridayDiscount * (decimal)1.7;
            }
            else if (item.Price <= 1000)
            {
                return (decimal)defaultFridayDiscount * (decimal)1.5;
            }
            else
            {
                return 0;
            }
        }

        static public bool isBlackFriday()
        {
            if (blackFridayDate > DateTime.Now.AddDays(-5) && blackFridayDate < DateTime.Now.AddDays(5))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static public bool isBlackFriday(DateTime date)
        {
            if (blackFridayDate.DayOfYear > (date.AddDays(-5)).DayOfYear && blackFridayDate.DayOfYear < date.AddDays(5).DayOfYear)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static public void SetFridayDiscount()
        {
            foreach (var item in Classes.EF.Context.VW_ProductList)
            {
                item.Price -= Math.Round(item.Price * getFridayDiscount(item), 2);
            }
        }

        static public void SetFridayDiscount(List<DB.VW_ProductList> list)
        {
            foreach (var item in list)
            {
                if (item.Price > 0)
                {
                    item.Price -= Math.Round(item.Price * getFridayDiscount(item), 2);
                }
                else
                {
                    item.Price = 0;
                }
            }
        }
    }
}
