USE [master]
GO
/****** Object:  Database [MaterialShop]    Script Date: 05.11.2023 13:42:37 ******/
CREATE DATABASE [MaterialShop]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MaterialShop', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\MaterialShop.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'MaterialShop_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\MaterialShop_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [MaterialShop] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MaterialShop].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MaterialShop] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MaterialShop] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MaterialShop] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MaterialShop] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MaterialShop] SET ARITHABORT OFF 
GO
ALTER DATABASE [MaterialShop] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [MaterialShop] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MaterialShop] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MaterialShop] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MaterialShop] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MaterialShop] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MaterialShop] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MaterialShop] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MaterialShop] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MaterialShop] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MaterialShop] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MaterialShop] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MaterialShop] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MaterialShop] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MaterialShop] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MaterialShop] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MaterialShop] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MaterialShop] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [MaterialShop] SET  MULTI_USER 
GO
ALTER DATABASE [MaterialShop] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MaterialShop] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MaterialShop] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MaterialShop] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MaterialShop] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [MaterialShop] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [MaterialShop] SET QUERY_STORE = OFF
GO
USE [MaterialShop]
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 05.11.2023 13:42:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductCategory](
	[Id] [smallint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MeasureUnit]    Script Date: 05.11.2023 13:42:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeasureUnit](
	[Id] [smallint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 05.11.2023 13:42:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[ProductCategoryId] [smallint] NULL,
	[Price] [decimal](10, 2) NOT NULL,
	[MeasureUnitId] [smallint] NULL,
	[WeightOfOne] [decimal](10, 2) NULL,
	[AmountInStock] [decimal](10, 2) NULL,
	[PhotoLink] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VW_ProductList]    Script Date: 05.11.2023 13:42:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_ProductList]
AS
SELECT        dbo.Product.Id, dbo.Product.Title, dbo.Product.Description, dbo.ProductCategory.Title AS Category, dbo.Product.Price, dbo.Product.WeightOfOne, dbo.Product.AmountInStock, dbo.Product.PhotoLink, 
                         dbo.MeasureUnit.Title AS MeasureUnit
FROM            dbo.Product LEFT OUTER JOIN
                         dbo.ProductCategory ON dbo.Product.ProductCategoryId = dbo.ProductCategory.Id LEFT OUTER JOIN
                         dbo.MeasureUnit ON dbo.Product.MeasureUnitId = dbo.MeasureUnit.Id
GO
/****** Object:  Table [dbo].[City]    Script Date: 05.11.2023 13:42:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeliveryType]    Script Date: 05.11.2023 13:42:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliveryType](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
	[ExtraFee] [decimal](3, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductSale]    Script Date: 05.11.2023 13:42:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductSale](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SaleId] [int] NULL,
	[ProductId] [int] NULL,
	[Quantity] [decimal](8, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductSubCategory]    Script Date: 05.11.2023 13:42:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductSubCategory](
	[Id] [smallint] IDENTITY(1,1) NOT NULL,
	[CategoryId] [smallint] NULL,
	[SubCategoryId] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sale]    Script Date: 05.11.2023 13:42:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sale](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SaleDate] [datetime] NOT NULL,
	[StaffId] [smallint] NULL,
	[Discount] [decimal](3, 2) NOT NULL,
	[DeliveryId] [tinyint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Staff]    Script Date: 05.11.2023 13:42:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff](
	[Id] [smallint] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Patromimyc] [nvarchar](100) NULL,
	[Phone] [varchar](24) NOT NULL,
	[Email] [nvarchar](150) NOT NULL,
	[BirthDate] [date] NOT NULL,
	[PositionId] [tinyint] NULL,
	[PassNum] [char](4) NOT NULL,
	[PassSeries] [char](6) NOT NULL,
	[AddedSalaryPercent] [decimal](3, 2) NULL,
	[PhotoId] [smallint] NULL,
	[Password] [nvarchar](20) NULL,
	[Login] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StaffPhoto]    Script Date: 05.11.2023 13:42:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StaffPhoto](
	[Id] [smallint] IDENTITY(1,1) NOT NULL,
	[PhotoLink] [nvarchar](400) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StaffPosition]    Script Date: 05.11.2023 13:42:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StaffPosition](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
	[Salary] [decimal](10, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 05.11.2023 13:42:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supplier](
	[Id] [smallint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
	[RepresentetiveName] [nvarchar](150) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Address] [nvarchar](100) NOT NULL,
	[Phone] [varchar](24) NOT NULL,
	[CityId] [tinyint] NULL,
	[PaymentAccount] [char](12) NOT NULL,
	[ITN] [char](12) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SupplyOrder]    Script Date: 05.11.2023 13:42:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupplyOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderNum] [nchar](20) NOT NULL,
	[SupplierId] [smallint] NULL,
	[ProductId] [int] NULL,
	[ProductQuantity] [decimal](8, 2) NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[CourierName] [nvarchar](150) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[City] ON 

INSERT [dbo].[City] ([Id], [Title]) VALUES (1, N'Москва')
INSERT [dbo].[City] ([Id], [Title]) VALUES (2, N'Санкт-Петербург')
INSERT [dbo].[City] ([Id], [Title]) VALUES (3, N'Тверь')
INSERT [dbo].[City] ([Id], [Title]) VALUES (4, N'Сергиев Посад')
INSERT [dbo].[City] ([Id], [Title]) VALUES (5, N'Ярославль')
INSERT [dbo].[City] ([Id], [Title]) VALUES (6, N'Ижевск')
SET IDENTITY_INSERT [dbo].[City] OFF
GO
SET IDENTITY_INSERT [dbo].[DeliveryType] ON 

INSERT [dbo].[DeliveryType] ([Id], [Title], [ExtraFee]) VALUES (1, N'Самовывоз', CAST(0.00 AS Decimal(3, 2)))
INSERT [dbo].[DeliveryType] ([Id], [Title], [ExtraFee]) VALUES (2, N'Доставка', CAST(0.10 AS Decimal(3, 2)))
INSERT [dbo].[DeliveryType] ([Id], [Title], [ExtraFee]) VALUES (3, N'Экспресс-доставка (<250 кг.)', CAST(0.20 AS Decimal(3, 2)))
INSERT [dbo].[DeliveryType] ([Id], [Title], [ExtraFee]) VALUES (4, N'Экспресс-доставка (>250 кг)', CAST(0.25 AS Decimal(3, 2)))
SET IDENTITY_INSERT [dbo].[DeliveryType] OFF
GO
SET IDENTITY_INSERT [dbo].[MeasureUnit] ON 

INSERT [dbo].[MeasureUnit] ([Id], [Title]) VALUES (1, N'метр')
INSERT [dbo].[MeasureUnit] ([Id], [Title]) VALUES (2, N'сантиметр')
INSERT [dbo].[MeasureUnit] ([Id], [Title]) VALUES (3, N'миллиметр')
INSERT [dbo].[MeasureUnit] ([Id], [Title]) VALUES (4, N'дюйм')
INSERT [dbo].[MeasureUnit] ([Id], [Title]) VALUES (5, N'кв. метр')
INSERT [dbo].[MeasureUnit] ([Id], [Title]) VALUES (6, N'литр')
INSERT [dbo].[MeasureUnit] ([Id], [Title]) VALUES (7, N'кубометр')
INSERT [dbo].[MeasureUnit] ([Id], [Title]) VALUES (8, N'единица товара')
INSERT [dbo].[MeasureUnit] ([Id], [Title]) VALUES (9, N'килограмм')
SET IDENTITY_INSERT [dbo].[MeasureUnit] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (1, N'Настольная лампа', N'', 21, CAST(3907.00 AS Decimal(10, 2)), 8, CAST(2637.99 AS Decimal(10, 2)), CAST(55.00 AS Decimal(10, 2)), N'/ProdPhoto/лампа.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (2, N'Шкаф многофункциональный', N'', 23, CAST(527.00 AS Decimal(10, 2)), 8, CAST(4069.99 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), N'/ProdPhoto/шкаф.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (3, N'Прикроватная тумбочка', N'', 23, CAST(2937.00 AS Decimal(10, 2)), 8, CAST(89.37 AS Decimal(10, 2)), CAST(15.00 AS Decimal(10, 2)), N'/ProdPhoto/шкаф.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (4, N'Картина "Утренний пейзаж"', N'', 23, CAST(2010.00 AS Decimal(10, 2)), 8, CAST(321.11 AS Decimal(10, 2)), CAST(18.00 AS Decimal(10, 2)), N'/ProdPhoto/картина.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (5, N'Стол обеденный', N'', 23, CAST(2069.00 AS Decimal(10, 2)), 8, CAST(4776.73 AS Decimal(10, 2)), CAST(85.00 AS Decimal(10, 2)), N'/ProdPhoto/стол.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (6, N'Обои флезелиновые', N'', 22, CAST(1268.00 AS Decimal(10, 2)), 5, CAST(4047.63 AS Decimal(10, 2)), CAST(89.00 AS Decimal(10, 2)), N'/ProdPhoto/обои.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (7, N'Обои бумажные с текстурой', N'', 22, CAST(3222.00 AS Decimal(10, 2)), 5, CAST(2386.95 AS Decimal(10, 2)), CAST(134.00 AS Decimal(10, 2)), N'/ProdPhoto/обои.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (8, N'Настенная панель', N'', 22, CAST(2066.00 AS Decimal(10, 2)), 5, CAST(4354.68 AS Decimal(10, 2)), CAST(132.00 AS Decimal(10, 2)), N'/ProdPhoto/обои.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (9, N'Обои бумажные "Ромашка"', N'', 22, CAST(653.00 AS Decimal(10, 2)), 5, CAST(4049.28 AS Decimal(10, 2)), CAST(134.00 AS Decimal(10, 2)), N'/ProdPhoto/обои.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (10, N'Ленолиум', N'', 21, CAST(3219.00 AS Decimal(10, 2)), 5, CAST(4318.84 AS Decimal(10, 2)), CAST(6.00 AS Decimal(10, 2)), N'/ProdPhoto/ленолиум.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (11, N'Ламинат темный', N'', 21, CAST(3471.00 AS Decimal(10, 2)), 5, CAST(1308.45 AS Decimal(10, 2)), CAST(83.00 AS Decimal(10, 2)), N'/ProdPhoto/паркет.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (12, N'Парктеная доска "Дуб"', N'', 21, CAST(3491.00 AS Decimal(10, 2)), 5, CAST(4141.35 AS Decimal(10, 2)), CAST(9.00 AS Decimal(10, 2)), N'/ProdPhoto/паркет.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (13, N'Окно двухстворчатое "Карлина"', N'', 20, CAST(1866.00 AS Decimal(10, 2)), 8, CAST(3110.02 AS Decimal(10, 2)), CAST(35.00 AS Decimal(10, 2)), N'/ProdPhoto/окно.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (14, N'Окно полноразмерное', N'', 20, CAST(3252.00 AS Decimal(10, 2)), 8, CAST(3645.40 AS Decimal(10, 2)), CAST(98.00 AS Decimal(10, 2)), N'/ProdPhoto/окно.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (15, N'Окно-форточка', N'', 20, CAST(171.00 AS Decimal(10, 2)), 8, CAST(1296.13 AS Decimal(10, 2)), CAST(104.00 AS Decimal(10, 2)), N'/ProdPhoto/окно.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (16, N'Окно деревянное "Простота"', N'', 20, CAST(2477.00 AS Decimal(10, 2)), 8, CAST(261.89 AS Decimal(10, 2)), CAST(97.00 AS Decimal(10, 2)), N'/ProdPhoto/окно.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (17, N'Окно в стиле лофт', N'', 20, CAST(2721.00 AS Decimal(10, 2)), 8, CAST(192.66 AS Decimal(10, 2)), CAST(35.00 AS Decimal(10, 2)), N'/ProdPhoto/окно.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (18, N'Двери межкомнатные', N'', 19, CAST(2348.00 AS Decimal(10, 2)), 8, CAST(2305.43 AS Decimal(10, 2)), CAST(79.00 AS Decimal(10, 2)), N'/ProdPhoto/дверь.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (19, N'Двери межкомнатные с непрозрачными вставками', N'', 19, CAST(445.00 AS Decimal(10, 2)), 8, CAST(3167.10 AS Decimal(10, 2)), CAST(23.00 AS Decimal(10, 2)), N'/ProdPhoto/дверь.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (20, N'Входная дверь "Ахиллесс"', N'', 19, CAST(1430.00 AS Decimal(10, 2)), 8, CAST(1356.28 AS Decimal(10, 2)), CAST(35.00 AS Decimal(10, 2)), N'/ProdPhoto/дверь.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (21, N'Аккумулятор переносной', N'', 18, CAST(2469.00 AS Decimal(10, 2)), 8, CAST(2447.08 AS Decimal(10, 2)), CAST(123.00 AS Decimal(10, 2)), N'/ProdPhoto/аккумулятор.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (22, N'Набор батарек АА 16 штук', N'', 18, CAST(1257.00 AS Decimal(10, 2)), 8, CAST(4500.51 AS Decimal(10, 2)), CAST(140.00 AS Decimal(10, 2)), N'/ProdPhoto/аккумулятор.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (23, N'Клемы для зарядки', N'', 18, CAST(417.00 AS Decimal(10, 2)), 8, CAST(576.32 AS Decimal(10, 2)), CAST(130.00 AS Decimal(10, 2)), N'/ProdPhoto/аккумулятор.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (24, N'Лампочка светодеоидная', N'', 17, CAST(396.00 AS Decimal(10, 2)), 8, CAST(3447.37 AS Decimal(10, 2)), CAST(20.00 AS Decimal(10, 2)), N'/ProdPhoto/лампочка.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (25, N'Лампочка энергосберегающая 4,5 ВТ', N'', 17, CAST(4719.00 AS Decimal(10, 2)), 8, CAST(4279.05 AS Decimal(10, 2)), CAST(49.00 AS Decimal(10, 2)), N'/ProdPhoto/лампочка.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (26, N'Лампа накаливая Е27 220 ВТ', N'', 17, CAST(2133.00 AS Decimal(10, 2)), 8, CAST(1031.13 AS Decimal(10, 2)), CAST(21.00 AS Decimal(10, 2)), N'/ProdPhoto/лампочка.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (27, N'Патрон для лампы Е27', N'', 17, CAST(3488.00 AS Decimal(10, 2)), 8, CAST(1407.64 AS Decimal(10, 2)), CAST(1.00 AS Decimal(10, 2)), N'/ProdPhoto/лампочка.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (28, N'Светильник настенный светодиодный Hesler', N'', 17, CAST(4973.00 AS Decimal(10, 2)), 8, CAST(528.44 AS Decimal(10, 2)), CAST(53.00 AS Decimal(10, 2)), N'/ProdPhoto/лампочка.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (29, N'Витая пара UTP 4PR', N'', 16, CAST(4439.00 AS Decimal(10, 2)), 1, CAST(4333.19 AS Decimal(10, 2)), CAST(79.00 AS Decimal(10, 2)), N'/ProdPhoto/кабель.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (30, N'Кабель Кабекс ВВГ-Пнг', N'', 16, CAST(654.00 AS Decimal(10, 2)), 1, CAST(330.30 AS Decimal(10, 2)), CAST(3.00 AS Decimal(10, 2)), N'/ProdPhoto/кабель.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (31, N'Розетка щитовая модульная', N'', 15, CAST(2692.00 AS Decimal(10, 2)), 8, CAST(960.33 AS Decimal(10, 2)), CAST(140.00 AS Decimal(10, 2)), N'/ProdPhoto/розетка.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (32, N'Розетка двойная открытая установка', N'', 15, CAST(1661.00 AS Decimal(10, 2)), 8, CAST(1857.34 AS Decimal(10, 2)), CAST(140.00 AS Decimal(10, 2)), N'/ProdPhoto/розетка.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (33, N'Розетка крытая белая с заземлением', N'', 15, CAST(4620.00 AS Decimal(10, 2)), 8, CAST(1633.58 AS Decimal(10, 2)), CAST(147.00 AS Decimal(10, 2)), N'/ProdPhoto/розетка.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (34, N'Выключатель одноклавишный', N'', 15, CAST(4143.00 AS Decimal(10, 2)), 8, CAST(100.87 AS Decimal(10, 2)), CAST(94.00 AS Decimal(10, 2)), N'/ProdPhoto/розетка.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (35, N'Камень искусственнный LivingStone', N'', 14, CAST(2295.00 AS Decimal(10, 2)), 5, CAST(4505.59 AS Decimal(10, 2)), CAST(38.00 AS Decimal(10, 2)), N'/ProdPhoto/розетка.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (36, N'Клей-герметик', N'', 14, CAST(1043.00 AS Decimal(10, 2)), 7, CAST(1223.61 AS Decimal(10, 2)), CAST(148.00 AS Decimal(10, 2)), N'/ProdPhoto/клей.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (37, N'Утеплитель Стандарт 50 мм', N'', 14, CAST(4169.00 AS Decimal(10, 2)), 5, CAST(3693.27 AS Decimal(10, 2)), CAST(37.00 AS Decimal(10, 2)), N'/ProdPhoto/мешок песок.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (38, N'Пескобетон Axton', N'', 14, CAST(3690.00 AS Decimal(10, 2)), 9, CAST(2489.57 AS Decimal(10, 2)), CAST(4.00 AS Decimal(10, 2)), N'/ProdPhoto/мешок песок.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (39, N'Гипсокартон влагостойкий', N'', 13, CAST(1901.00 AS Decimal(10, 2)), 5, CAST(1730.58 AS Decimal(10, 2)), CAST(78.00 AS Decimal(10, 2)), N'/ProdPhoto/рубероид.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (40, N'Штукатура гипсовая Knuuf', N'', 13, CAST(4953.00 AS Decimal(10, 2)), 9, CAST(3875.14 AS Decimal(10, 2)), CAST(78.00 AS Decimal(10, 2)), N'/ProdPhoto/мешок песок.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (41, N'Наливной пол "Горизонт"', N'', 13, CAST(579.00 AS Decimal(10, 2)), 9, CAST(80.50 AS Decimal(10, 2)), CAST(150.00 AS Decimal(10, 2)), N'/ProdPhoto/мешок песок.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (42, N'Профнастил С8 0,35 мм', N'', 13, CAST(3512.00 AS Decimal(10, 2)), 5, CAST(781.83 AS Decimal(10, 2)), CAST(10.00 AS Decimal(10, 2)), N'/ProdPhoto/рубероид.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (43, N'Рубероид РКП-350', N'', 13, CAST(4915.00 AS Decimal(10, 2)), 5, CAST(1674.57 AS Decimal(10, 2)), CAST(124.00 AS Decimal(10, 2)), N'/ProdPhoto/рубероид.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (44, N'Серпянка UNIBOB 50x20', N'', 13, CAST(4990.00 AS Decimal(10, 2)), 1, CAST(273.41 AS Decimal(10, 2)), CAST(27.00 AS Decimal(10, 2)), N'/ProdPhoto/рубероид.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (45, N'Профиль потолочный направляющий', N'', 13, CAST(4020.00 AS Decimal(10, 2)), 1, CAST(4486.68 AS Decimal(10, 2)), CAST(8.00 AS Decimal(10, 2)), N'/ProdPhoto/инструмент.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (46, N'Шпаклевка полимерная финишная Vetonit', N'', 13, CAST(1679.00 AS Decimal(10, 2)), 9, CAST(444.10 AS Decimal(10, 2)), CAST(117.00 AS Decimal(10, 2)), N'/ProdPhoto/мешок песок.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (47, N'Брусок строганный', N'', 11, CAST(4044.00 AS Decimal(10, 2)), 2, CAST(1761.52 AS Decimal(10, 2)), CAST(133.00 AS Decimal(10, 2)), N'/ProdPhoto/доска.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (48, N'Доска строганная', N'', 11, CAST(4866.00 AS Decimal(10, 2)), 1, CAST(1749.55 AS Decimal(10, 2)), CAST(126.00 AS Decimal(10, 2)), N'/ProdPhoto/доска.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (49, N'Плита ОСП-3 "Муром"', N'', 11, CAST(2862.00 AS Decimal(10, 2)), 5, CAST(168.04 AS Decimal(10, 2)), CAST(31.00 AS Decimal(10, 2)), N'/ProdPhoto/лампа.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (50, N'Кисть плоская натуральная щетина', N'', 10, CAST(4156.00 AS Decimal(10, 2)), 8, CAST(1552.05 AS Decimal(10, 2)), CAST(70.00 AS Decimal(10, 2)), N'/ProdPhoto/инструмент.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (51, N'Валик полиакрил 250 мм', N'', 10, CAST(3305.00 AS Decimal(10, 2)), 8, CAST(725.39 AS Decimal(10, 2)), CAST(143.00 AS Decimal(10, 2)), N'/ProdPhoto/инструмент.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (52, N'Кисть плоска смешанная щетина', N'', 10, CAST(2069.00 AS Decimal(10, 2)), 8, CAST(746.72 AS Decimal(10, 2)), CAST(10.00 AS Decimal(10, 2)), N'/ProdPhoto/инструмент.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (53, N'Шпатель малярный Henzo', N'', 10, CAST(1481.00 AS Decimal(10, 2)), 8, CAST(2258.93 AS Decimal(10, 2)), CAST(129.00 AS Decimal(10, 2)), N'/ProdPhoto/инструмент.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (54, N'Правило аллюминивое', N'', 10, CAST(1577.00 AS Decimal(10, 2)), 8, CAST(183.15 AS Decimal(10, 2)), CAST(57.00 AS Decimal(10, 2)), N'/ProdPhoto/инструмент.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (55, N'Лестиница раскладая деревянная "Силач" 3,2 м', N'', 9, CAST(4643.00 AS Decimal(10, 2)), 8, CAST(3414.29 AS Decimal(10, 2)), CAST(140.00 AS Decimal(10, 2)), N'/ProdPhoto/инструмент.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (56, N'Стремянка стальная 7 ступеней 1,26 м', N'', 9, CAST(650.00 AS Decimal(10, 2)), 8, CAST(2250.35 AS Decimal(10, 2)), CAST(74.00 AS Decimal(10, 2)), N'/ProdPhoto/инструмент.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (57, N'Стол малярный "Новая высота"', N'', 9, CAST(4809.00 AS Decimal(10, 2)), 8, CAST(2512.78 AS Decimal(10, 2)), CAST(124.00 AS Decimal(10, 2)), N'/ProdPhoto/стол.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (58, N'Лестница аллюминиевая приставная', N'', 9, CAST(1368.00 AS Decimal(10, 2)), 8, CAST(2416.24 AS Decimal(10, 2)), CAST(4.00 AS Decimal(10, 2)), N'/ProdPhoto/инструмент.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (59, N'Перчатки латексные', N'', 8, CAST(1145.00 AS Decimal(10, 2)), 8, CAST(4067.79 AS Decimal(10, 2)), CAST(48.00 AS Decimal(10, 2)), N'/ProdPhoto/костюм.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (60, N'Очки защитные пластиковые', N'', 8, CAST(223.00 AS Decimal(10, 2)), 8, CAST(2021.12 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), N'/ProdPhoto/костюм.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (61, N'Комбинизон малярный защитный', N'', 8, CAST(2040.00 AS Decimal(10, 2)), 8, CAST(2385.17 AS Decimal(10, 2)), CAST(14.00 AS Decimal(10, 2)), N'/ProdPhoto/костюм.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (62, N'Респиратор защитный', N'', 8, CAST(3565.00 AS Decimal(10, 2)), 8, CAST(2928.87 AS Decimal(10, 2)), CAST(134.00 AS Decimal(10, 2)), N'/ProdPhoto/костюм.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (63, N'Ножовка по гипсокартону', N'', 7, CAST(605.00 AS Decimal(10, 2)), 8, CAST(1784.46 AS Decimal(10, 2)), CAST(84.00 AS Decimal(10, 2)), N'/ProdPhoto/ножовка.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (64, N'Ножовка по дереву', N'', 7, CAST(1318.00 AS Decimal(10, 2)), 8, CAST(1717.59 AS Decimal(10, 2)), CAST(28.00 AS Decimal(10, 2)), N'/ProdPhoto/ножовка.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (65, N'Стамеска Hesler', N'', 7, CAST(4941.00 AS Decimal(10, 2)), 8, CAST(3515.41 AS Decimal(10, 2)), CAST(99.00 AS Decimal(10, 2)), N'/ProdPhoto/инструмент.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (66, N'Шлаг гибкий', N'', 6, CAST(246.00 AS Decimal(10, 2)), 1, CAST(4137.49 AS Decimal(10, 2)), CAST(36.00 AS Decimal(10, 2)), N'/ProdPhoto/шланг.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (67, N'Садовая лопатка', N'', 6, CAST(4551.00 AS Decimal(10, 2)), 8, CAST(2092.74 AS Decimal(10, 2)), CAST(6.00 AS Decimal(10, 2)), N'/ProdPhoto/лопатка.jpg')
INSERT [dbo].[Product] ([Id], [Title], [Description], [ProductCategoryId], [Price], [MeasureUnitId], [WeightOfOne], [AmountInStock], [PhotoLink]) VALUES (68, N'Удобрение азотное "Гном"', N'', 6, CAST(2127.00 AS Decimal(10, 2)), 9, CAST(4258.53 AS Decimal(10, 2)), CAST(78.00 AS Decimal(10, 2)), N'/ProdPhoto/мешок песок.jpg')
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductCategory] ON 

INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (1, N'Инструменты')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (2, N'Материалы')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (3, N'Запчасти')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (4, N'Электрика')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (5, N'Интерьер и декор')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (6, N'Садовый интрумент')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (7, N'Столярный инструмент')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (8, N'Защита')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (9, N'Лестницы')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (10, N'Штукатурно-малярный инструмент')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (11, N'Пиломатериалы')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (12, N'Металл и изделия')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (13, N'Отделочные материалы')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (14, N'Строительные материалы')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (15, N'Розетки, выключатели')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (16, N'Кабели, провода')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (17, N'Освещение')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (18, N'Источники питания')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (19, N'Двери')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (20, N'Окна')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (21, N'Напольные покрытия')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (22, N'Обои')
INSERT [dbo].[ProductCategory] ([Id], [Title]) VALUES (23, N'Декор для дома')
SET IDENTITY_INSERT [dbo].[ProductCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductSale] ON 

INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (1, 3, 56, CAST(9.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (2, 30, 8, CAST(8.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (3, 15, 51, CAST(1.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (4, 18, 57, CAST(9.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (5, 24, 22, CAST(10.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (6, 41, 6, CAST(10.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (7, 22, 11, CAST(8.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (8, 4, 20, CAST(1.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (9, 1, 55, CAST(4.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (10, 5, 5, CAST(8.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (11, 14, 58, CAST(7.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (12, 9, 64, CAST(10.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (13, 38, 15, CAST(1.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (14, 33, 38, CAST(8.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (15, 18, 18, CAST(7.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (16, 4, 55, CAST(3.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (17, 45, 15, CAST(10.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (18, 36, 56, CAST(9.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (19, 41, 39, CAST(2.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (20, 6, 53, CAST(3.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (21, 33, 37, CAST(7.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (22, 3, 40, CAST(6.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (23, 37, 68, CAST(1.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (24, 25, 65, CAST(9.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (25, 25, 33, CAST(2.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (26, 24, 62, CAST(9.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (27, 29, 61, CAST(4.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (28, 36, 10, CAST(4.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (29, 4, 44, CAST(8.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (30, 28, 54, CAST(4.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (31, 40, 11, CAST(9.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (32, 33, 66, CAST(4.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (33, 36, 28, CAST(8.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (34, 23, 22, CAST(6.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (35, 38, 14, CAST(7.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (36, 9, 10, CAST(10.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (37, 31, 46, CAST(10.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (38, 8, 55, CAST(5.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (39, 4, 5, CAST(2.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (40, 18, 24, CAST(9.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (41, 41, 21, CAST(3.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (42, 37, 55, CAST(7.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (43, 43, 36, CAST(5.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (44, 20, 32, CAST(4.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (45, 18, 35, CAST(4.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (46, 13, 9, CAST(6.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (47, 31, 56, CAST(8.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (48, 45, 46, CAST(1.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (49, 4, 67, CAST(3.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (50, 2, 26, CAST(9.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (51, 36, 38, CAST(10.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (52, 20, 16, CAST(4.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (53, 29, 9, CAST(5.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (54, 19, 3, CAST(7.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (55, 15, 38, CAST(8.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (56, 36, 25, CAST(3.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (57, 21, 38, CAST(10.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (58, 38, 42, CAST(3.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (59, 23, 58, CAST(10.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (60, 2, 42, CAST(3.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (61, 8, 30, CAST(1.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (62, 40, 38, CAST(1.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (63, 30, 39, CAST(6.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (64, 39, 3, CAST(9.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (65, 43, 12, CAST(2.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (66, 34, 13, CAST(10.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (67, 22, 67, CAST(5.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (68, 37, 4, CAST(10.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (69, 40, 67, CAST(7.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (70, 40, 63, CAST(8.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (71, 23, 57, CAST(7.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (72, 16, 20, CAST(1.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (73, 10, 10, CAST(4.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (74, 3, 17, CAST(10.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (75, 29, 43, CAST(6.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (76, 31, 58, CAST(4.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (77, 30, 1, CAST(8.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (78, 27, 31, CAST(3.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (79, 9, 14, CAST(9.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (80, 23, 63, CAST(4.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (81, 46, 1, CAST(1.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (82, 46, 3, CAST(4.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (83, 46, 2, CAST(1.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (84, 46, 7, CAST(0.40 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (85, 47, 1, CAST(4.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (86, 48, 2, CAST(7.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (87, 48, 1, CAST(1.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (88, 48, 6, CAST(1.20 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (89, 49, 2, CAST(1.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (90, 49, 1, CAST(4.00 AS Decimal(8, 2)))
INSERT [dbo].[ProductSale] ([Id], [SaleId], [ProductId], [Quantity]) VALUES (91, 49, 6, CAST(0.80 AS Decimal(8, 2)))
SET IDENTITY_INSERT [dbo].[ProductSale] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductSubCategory] ON 

INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (1, 1, 6)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (2, 1, 7)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (3, 1, 8)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (4, 1, 9)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (5, 1, 10)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (6, 2, 11)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (7, 2, 12)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (8, 2, 13)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (9, 2, 14)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (10, 4, 15)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (11, 4, 16)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (12, 4, 17)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (13, 4, 18)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (14, 5, 19)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (15, 5, 20)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (16, 5, 21)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (17, 5, 22)
INSERT [dbo].[ProductSubCategory] ([Id], [CategoryId], [SubCategoryId]) VALUES (18, 5, 23)
SET IDENTITY_INSERT [dbo].[ProductSubCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[Sale] ON 

INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (1, CAST(N'2016-01-07T09:18:00.000' AS DateTime), 4, CAST(0.41 AS Decimal(3, 2)), 1)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (2, CAST(N'2013-01-17T02:47:00.000' AS DateTime), 23, CAST(0.39 AS Decimal(3, 2)), 3)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (3, CAST(N'2023-04-22T23:16:00.000' AS DateTime), 15, CAST(0.31 AS Decimal(3, 2)), 3)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (4, CAST(N'2014-08-08T10:55:00.000' AS DateTime), 5, CAST(0.06 AS Decimal(3, 2)), 1)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (5, CAST(N'2014-09-21T10:51:00.000' AS DateTime), 20, CAST(0.50 AS Decimal(3, 2)), 2)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (6, CAST(N'2022-02-20T16:32:00.000' AS DateTime), 16, CAST(0.18 AS Decimal(3, 2)), 3)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (7, CAST(N'2015-09-15T17:46:00.000' AS DateTime), 38, CAST(0.43 AS Decimal(3, 2)), 4)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (8, CAST(N'2005-01-10T17:52:00.000' AS DateTime), 42, CAST(0.12 AS Decimal(3, 2)), 3)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (9, CAST(N'2023-05-04T10:13:00.000' AS DateTime), 29, CAST(0.05 AS Decimal(3, 2)), 2)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (10, CAST(N'2013-03-26T02:44:00.000' AS DateTime), 9, CAST(0.08 AS Decimal(3, 2)), 3)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (11, CAST(N'2014-04-01T14:08:00.000' AS DateTime), 22, CAST(0.39 AS Decimal(3, 2)), 4)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (12, CAST(N'2008-10-10T06:22:00.000' AS DateTime), 5, CAST(0.36 AS Decimal(3, 2)), 1)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (13, CAST(N'2020-08-03T09:25:00.000' AS DateTime), 23, CAST(0.47 AS Decimal(3, 2)), 1)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (14, CAST(N'2013-01-09T08:54:00.000' AS DateTime), 26, CAST(0.06 AS Decimal(3, 2)), 1)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (15, CAST(N'2016-01-28T19:40:00.000' AS DateTime), 37, CAST(0.09 AS Decimal(3, 2)), 2)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (16, CAST(N'2006-07-01T15:53:00.000' AS DateTime), 17, CAST(0.49 AS Decimal(3, 2)), 4)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (17, CAST(N'2005-03-05T12:59:00.000' AS DateTime), 43, CAST(0.19 AS Decimal(3, 2)), 2)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (18, CAST(N'2010-09-20T13:28:00.000' AS DateTime), 49, CAST(0.02 AS Decimal(3, 2)), 4)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (19, CAST(N'2005-09-28T04:25:00.000' AS DateTime), 6, CAST(0.43 AS Decimal(3, 2)), 1)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (20, CAST(N'2015-12-03T18:39:00.000' AS DateTime), 15, CAST(0.45 AS Decimal(3, 2)), 4)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (21, CAST(N'2014-11-14T23:22:00.000' AS DateTime), 2, CAST(0.29 AS Decimal(3, 2)), 3)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (22, CAST(N'2015-08-17T20:14:00.000' AS DateTime), 21, CAST(0.27 AS Decimal(3, 2)), 2)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (23, CAST(N'2008-04-25T05:36:00.000' AS DateTime), 25, CAST(0.29 AS Decimal(3, 2)), 4)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (24, CAST(N'2023-08-20T09:47:00.000' AS DateTime), 14, CAST(0.01 AS Decimal(3, 2)), 3)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (25, CAST(N'2011-05-26T18:06:00.000' AS DateTime), 8, CAST(0.17 AS Decimal(3, 2)), 2)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (26, CAST(N'2015-09-08T00:55:00.000' AS DateTime), 12, CAST(0.30 AS Decimal(3, 2)), 4)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (27, CAST(N'2018-06-30T03:15:00.000' AS DateTime), 45, CAST(0.07 AS Decimal(3, 2)), 4)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (28, CAST(N'2015-12-03T06:15:00.000' AS DateTime), 50, CAST(0.48 AS Decimal(3, 2)), 1)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (29, CAST(N'2014-09-02T12:13:00.000' AS DateTime), 34, CAST(0.17 AS Decimal(3, 2)), 3)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (30, CAST(N'2018-05-05T05:27:00.000' AS DateTime), 29, CAST(0.26 AS Decimal(3, 2)), 4)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (31, CAST(N'2018-03-17T22:58:00.000' AS DateTime), 45, CAST(0.15 AS Decimal(3, 2)), 3)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (32, CAST(N'2022-09-29T20:07:00.000' AS DateTime), 17, CAST(0.19 AS Decimal(3, 2)), 1)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (33, CAST(N'2009-02-08T08:15:00.000' AS DateTime), 37, CAST(0.07 AS Decimal(3, 2)), 3)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (34, CAST(N'2008-02-08T16:41:00.000' AS DateTime), 5, CAST(0.40 AS Decimal(3, 2)), 4)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (35, CAST(N'2008-12-28T02:16:00.000' AS DateTime), 13, CAST(0.49 AS Decimal(3, 2)), 4)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (36, CAST(N'2013-06-02T03:43:00.000' AS DateTime), 33, CAST(0.40 AS Decimal(3, 2)), 1)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (37, CAST(N'2019-08-04T12:47:00.000' AS DateTime), 30, CAST(0.41 AS Decimal(3, 2)), 3)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (38, CAST(N'2009-09-21T00:38:00.000' AS DateTime), 9, CAST(0.29 AS Decimal(3, 2)), 2)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (39, CAST(N'2019-04-22T17:13:00.000' AS DateTime), 35, CAST(0.48 AS Decimal(3, 2)), 1)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (40, CAST(N'2017-10-05T12:03:00.000' AS DateTime), 10, CAST(0.18 AS Decimal(3, 2)), 4)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (41, CAST(N'2007-12-14T00:06:00.000' AS DateTime), 5, CAST(0.35 AS Decimal(3, 2)), 2)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (42, CAST(N'2009-01-05T08:12:00.000' AS DateTime), 50, CAST(0.15 AS Decimal(3, 2)), 1)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (43, CAST(N'2008-01-24T23:48:00.000' AS DateTime), 6, CAST(0.29 AS Decimal(3, 2)), 3)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (44, CAST(N'2021-05-28T19:05:00.000' AS DateTime), 32, CAST(0.18 AS Decimal(3, 2)), 2)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (45, CAST(N'2022-05-23T20:52:00.000' AS DateTime), 49, CAST(0.42 AS Decimal(3, 2)), 2)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (46, CAST(N'2023-10-31T20:28:06.217' AS DateTime), 32, CAST(0.05 AS Decimal(3, 2)), NULL)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (47, CAST(N'2023-11-01T22:45:57.483' AS DateTime), 32, CAST(0.00 AS Decimal(3, 2)), 1)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (48, CAST(N'2023-11-04T10:38:30.967' AS DateTime), 32, CAST(0.05 AS Decimal(3, 2)), 1)
INSERT [dbo].[Sale] ([Id], [SaleDate], [StaffId], [Discount], [DeliveryId]) VALUES (49, CAST(N'2023-11-05T11:34:37.817' AS DateTime), 32, CAST(0.45 AS Decimal(3, 2)), 1)
SET IDENTITY_INSERT [dbo].[Sale] OFF
GO
SET IDENTITY_INSERT [dbo].[Staff] ON 

INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (1, N'Тихомирова', N'Ксения', N'Петровна', N'7(259)721-87-42', N'napek_okevu40@outlook.com', CAST(N'2004-12-05' AS Date), 4, N'9656', N'943534', NULL, 29, N'Q03YJIW2', N'XLWOOWOYNY.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (2, N'Калмыков', N'Иван', N'Макарович', N'7(7669)059-81-08', N'yewab_ijebe93@outlook.com', CAST(N'1994-11-08' AS Date), 2, N'7821', N'341136', NULL, 47, N'H6RYA6WI', N'PIAQZYCMVO.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (3, N'Макаров', N'Владимир', N'Адрианович', N'7(56)055-20-48', N'coto_junoma7@hotmail.com', CAST(N'1994-10-29' AS Date), 4, N'2881', N'137137', NULL, 42, N'4', N'4')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (4, N'Матвеев', N'Захар', N'Матвеевич', N'7(6964)910-57-08', N'xobonu-baho72@hotmail.com', CAST(N'1972-03-15' AS Date), 5, N'2122', N'251930', NULL, 7, N'W5W3B4OH', N'IMWAKIKTNY.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (5, N'Иванова', N'Дарина', N'Макаровна', N'7(6878)583-18-47', N'fako-mozake7@outlook.com', CAST(N'1994-04-14' AS Date), 4, N'3957', N'154521', NULL, 28, N'VQNE2TR9', N'XSNZLXIMSI.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (6, N'Скворцов', N'Григорий', N'Максимович', N'7(789)764-30-83', N'pugul_ipeze84@outlook.com', CAST(N'1976-09-21' AS Date), 4, N'3697', N'319032', NULL, 12, N'QSH7544T', N'VNDEXWULMO.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (7, N'Журавлева', N'Ульяна', N'', N'7(99)087-66-44', N'kupef_ojisi17@hotmail.com', CAST(N'1980-03-26' AS Date), 1, N'4140', N'146223', NULL, 26, N'ZPPH6FN8', N'RARLYGMIDP.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (8, N'Шилов', N'Иван', N'Матвеевич', N'7(038)129-59-85', N'wuyova-vota62@yahoo.com', CAST(N'1971-11-17' AS Date), 4, N'3389', N'978747', NULL, 18, N'VKD8LUWF', N'FUGMVCJJTA.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (9, N'Матвеева', N'Анна', N'Давидовна', N'7(519)172-26-21', N'huyo-luzona8@yahoo.com', CAST(N'1993-10-20' AS Date), 4, N'8271', N'714162', NULL, 24, N'HDTHW6GO', N'HJWITWNWZI.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (10, N'Евдокимова', N'Елизавета', N'Александровна', N'7(0936)703-08-52', N'wahami_bupi89@hotmail.com', CAST(N'1983-05-26' AS Date), 2, N'5428', N'508933', NULL, 21, N'DQSJRP7Y', N'WVUNKVXALF.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (11, N'Лебедев', N'Михаил', N'Константинович', N'7(58)335-84-58', N'vidiba-xoru49@gmail.com', CAST(N'1971-01-06' AS Date), 5, N'1384', N'338959', NULL, 13, N'FGKEUKJE', N'GCSOQKDTTX.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (12, N'Румянцев', N'Николай', N'Романович', N'7(207)296-38-99', N'vereya_vado6@gmail.com', CAST(N'1979-08-10' AS Date), 4, N'3752', N'713924', NULL, 48, N'H55RW4LG', N'MZAAYNNODV.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (13, N'Семенова', N'Виктория', N'Даниэльевна', N'7(69)681-80-76', N'pasaxar_uga61@hotmail.com', CAST(N'2003-01-21' AS Date), 3, N'5699', N'191051', NULL, 9, N'EXT4GRF5', N'LABRNCGRMZ.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (14, N'Бычкова', N'Ника', N'Ильинична', N'7(00)942-79-48', N'lumu_kipiba34@gmail.com', CAST(N'1984-06-08' AS Date), 1, N'8312', N'729374', NULL, 41, N'X96RSDMH', N'MVXEMCQZKD.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (15, N'Морозова', N'Ксения', N'Глебовна', N'7(4160)593-75-32', N'madudi-taze89@aol.com', CAST(N'1979-07-25' AS Date), 1, N'3248', N'302264', NULL, 34, N'YPUX42YA', N'UAMPKAJFXU.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (16, N'Смирнова', N'Таисия', N'Дмитриевна', N'7(24)218-32-80', N'bodop-edoyu69@mail.com', CAST(N'1985-02-18' AS Date), 5, N'5754', N'507162', NULL, 25, N'2', N'2')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (17, N'Быков', N'Роман', N'Матвеевич', N'7(22)278-01-97', N'gas_etupuke83@outlook.com', CAST(N'1994-12-23' AS Date), 1, N'4937', N'454207', NULL, 23, N'Y0AU4Z7S', N'VHVJCFMYIQ.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (18, N'Егорова', N'Вероника', N'Кирилловна', N'7(32)796-12-83', N'rufehe_hega91@outlook.com', CAST(N'1991-04-12' AS Date), 5, N'2541', N'772851', NULL, 31, N'66LUS6KJ', N'VVMCFANHIN.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (19, N'Мальцева', N'Юлия', N'Павловна', N'7(0588)079-20-12', N'deme-royaxi98@aol.com', CAST(N'1994-01-01' AS Date), 3, N'8196', N'952895', NULL, 16, N'6BCBK8F0', N'QYMEANLNHI.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (20, N'Колесникова', N'Ксения', N'Ивановна', N'7(80)000-13-22', N'bov-eyigiza44@aol.com', CAST(N'1976-02-11' AS Date), 1, N'6916', N'597528', NULL, 43, N'LPYBUQXY', N'RIKIIINBHU.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (21, N'Николаева', N'Виктория', N'Михайловна', N'7(28)475-18-10', N'buy_imuroco68@gmail.com', CAST(N'1986-10-17' AS Date), 1, N'2998', N'291505', NULL, 36, N'MKM92S9T', N'PRNPMYHJKU.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (22, N'Тарасова', N'Таисия', N'Александровна', N'7(0149)416-70-70', N'zop_ejixelu89@outlook.com', CAST(N'1974-08-24' AS Date), 3, N'9142', N'456784', NULL, 19, N'HHHP71PY', N'CHTOLACUFN.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (23, N'Егоров', N'Дмитрий', N'Матвеевич', N'7(71)143-27-24', N'zuxaseg-ada14@hotmail.com', CAST(N'1990-11-20' AS Date), 4, N'4785', N'293307', NULL, 11, N'MK3ZA0VI', N'RZSPXCLTDE.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (24, N'Лебедев', N'Евгений', N'Михайлович', N'7(09)814-51-69', N'luwuk-ulefu79@outlook.com', CAST(N'2003-03-29' AS Date), 1, N'2235', N'853606', NULL, 1, N'S82A4TS9', N'SJUHNHUJQP.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (25, N'Федоров', N'Максим', N'Кириллович', N'7(04)712-42-22', N'lifun-uraku3@hotmail.com', CAST(N'1980-09-14' AS Date), 2, N'2051', N'652933', NULL, 6, N'ZZ49IMUJ', N'AASJPLWPXA.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (26, N'Зуев', N'Даниил', N'', N'7(338)660-08-10', N'moxa-dijite22@aol.com', CAST(N'1999-01-02' AS Date), 2, N'7030', N'786093', NULL, 15, N'6LCT5H1F', N'FGTBSIMCPI.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (27, N'Дьяконова', N'Яна', N'Егоровна', N'7(91)522-43-38', N'rariw-ohoye53@yahoo.com', CAST(N'1981-07-22' AS Date), 3, N'1760', N'660165', NULL, 3, N'IS1RT2FK', N'AWTGJLJOCY.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (28, N'Корнев', N'Александр', N'Михайлович', N'7(007)308-73-16', N'xikufem-oto20@hotmail.com', CAST(N'1984-10-03' AS Date), 3, N'3447', N'523424', NULL, 5, N'WX5V4MD2', N'VWPVMDSJVM.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (29, N'Ефремова', N'Анастасия', N'Семёновна', N'7(72)813-83-39', N'hitupi_fiyo39@hotmail.com', CAST(N'1985-03-08' AS Date), 4, N'7286', N'124244', NULL, 14, N'2ULSGKGL', N'DPFRYDHDBN.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (30, N'Аксенов', N'Андрей', N'Даниилович', N'7(8223)621-62-92', N'yena-sebolu74@hotmail.com', CAST(N'1992-08-14' AS Date), 3, N'9846', N'163145', NULL, 4, N'6Z1MYABS', N'XBVVKJVLLY.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (31, N'Морозова', N'Аиша', N'Максимовна', N'7(904)215-93-78', N'fob_ikahepi84@aol.com', CAST(N'1986-02-28' AS Date), 2, N'8614', N'605013', NULL, 38, N'ZBPG3HZW', N'GHLDOXXXDG.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (32, N'Мартынов', N'Михаил', N'Владимирович', N'7(168)682-80-54', N'cuyo-pexege95@aol.com', CAST(N'2003-03-18' AS Date), 2, N'1059', N'415287', NULL, 30, N'1', N'1')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (33, N'Воронин', N'Савелий', N'Романович', N'7(7630)674-61-78', N'nowosup-uxi49@outlook.com', CAST(N'1984-12-06' AS Date), 4, N'9562', N'297635', NULL, 10, N'QJ5XL6U3', N'ELJDOSOQPO.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (34, N'Морозова', N'Кристина', N'', N'7(28)061-12-95', N'ruxu-rulefa29@gmail.com', CAST(N'1995-11-28' AS Date), 2, N'4582', N'591320', NULL, 40, N'O12YTZH1', N'LMRLGVXTBF.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (35, N'Селезнев', N'Мирослав', N'Даниилович', N'7(7888)252-89-24', N'vexo-konepe4@aol.com', CAST(N'1988-01-05' AS Date), 3, N'6291', N'301733', NULL, 17, N'921W63N8', N'ADUOZCOQSN.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (36, N'Смирнов', N'Егор', N'Олегович', N'7(981)693-97-99', N'xebopiv-ewo94@yahoo.com', CAST(N'1977-09-20' AS Date), 1, N'9969', N'518615', NULL, 46, N'ZJA47WZL', N'YSGVRYZUOP.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (37, N'Макарова', N'Милана', N'Ярославовна', N'7(007)667-54-59', N'yiruj-oweje35@yahoo.com', CAST(N'1998-03-02' AS Date), 2, N'3819', N'886626', NULL, 35, N'6PHO81YB', N'HFHLGTEGHA.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (38, N'Лобанов', N'Михаил', N'Ярославович', N'7(0160)054-96-55', N'bolucix_uzi76@aol.com', CAST(N'1983-04-04' AS Date), 3, N'1866', N'847238', NULL, 20, N'G2YQSTPN', N'ZQZTWGUIYH.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (39, N'Суслова', N'Дарина', N'Марковна', N'7(844)489-55-92', N'vumigit_owi22@mail.com', CAST(N'2001-04-12' AS Date), 4, N'1232', N'986791', NULL, 27, N'77UPM7I3', N'VZTFGDRJTM.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (40, N'Егоров', N'Максим', N'Егорович', N'7(67)164-27-13', N'gafab_acalo40@mail.com', CAST(N'1992-08-08' AS Date), 3, N'3527', N'729923', NULL, 49, N'AHOH3X5W', N'NWEDZXGFGE.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (41, N'Рыжов', N'Дмитрий', N'', N'7(18)151-61-01', N'hib_olarexi1@aol.com', CAST(N'1997-08-30' AS Date), 3, N'6768', N'777792', NULL, 8, N'1SYZ2IFW', N'XOFHTVEDXV.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (42, N'Сычева', N'Виктория', N'Ильинична', N'7(865)106-33-21', N'muwuka-leye24@gmail.com', CAST(N'1971-07-20' AS Date), 4, N'9790', N'721876', NULL, 22, N'P0P4WUVJ', N'PNPNXPLSAX.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (43, N'Прохорова', N'София', N'Михайловна', N'7(47)756-07-89', N'huyidom_aro81@mail.com', CAST(N'1990-10-13' AS Date), 2, N'2394', N'389790', NULL, 33, N'II3T58O3', N'PXGSZNSFIB.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (44, N'Гаврилова', N'Анна', N'Егоровна', N'7(944)558-23-57', N'wanav-usaso75@hotmail.com', CAST(N'1971-03-20' AS Date), 4, N'7774', N'556159', NULL, 32, N'3', N'3')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (45, N'Тарасова', N'Алиса', N'Сергеевна', N'7(68)010-12-49', N'wevehic-oko68@gmail.com', CAST(N'1990-07-28' AS Date), 4, N'9420', N'182718', NULL, 39, N'Y1V2553J', N'OQOANAYODJ.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (46, N'Коновалова', N'София', N'Ивановна', N'7(2443)225-26-09', N'cihap-avejo2@aol.com', CAST(N'1994-03-23' AS Date), 3, N'3150', N'699323', NULL, 44, N'TGI9YN6E', N'QMJGLKSURL.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (47, N'Белякова', N'Эмилия', N'Давидовна', N'7(57)921-09-97', N'bucuk_udafu74@hotmail.com', CAST(N'1998-07-23' AS Date), 5, N'6649', N'108891', NULL, 50, N'HGN6TFB7', N'RMEBJXVEWQ.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (48, N'Поляков', N'Тимур', N'', N'7(17)322-15-65', N'tesoyoc-ime65@outlook.com', CAST(N'1982-12-22' AS Date), 5, N'8978', N'132029', NULL, 37, N'4DD3X8ED', N'AARIBRPBMH.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (49, N'Евсеев', N'Лев', N'Егорович', N'7(9805)760-02-84', N'yovido-cimo2@outlook.com', CAST(N'1992-09-02' AS Date), 1, N'9317', N'700620', NULL, 2, N'JPCD5SHC', N'KINGTXXQVA.login')
INSERT [dbo].[Staff] ([Id], [FirstName], [LastName], [Patromimyc], [Phone], [Email], [BirthDate], [PositionId], [PassNum], [PassSeries], [AddedSalaryPercent], [PhotoId], [Password], [Login]) VALUES (50, N'Семенов', N'Гордей', N'Алексеевич', N'7(06)059-17-74', N'jovike-jilo89@mail.com', CAST(N'1985-03-23' AS Date), 3, N'5923', N'919341', NULL, 45, N'6T0QUDE4', N'BZITOKWEHR.login')
SET IDENTITY_INSERT [dbo].[Staff] OFF
GO
SET IDENTITY_INSERT [dbo].[StaffPhoto] ON 

INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (1, N'2pkTpZsXc1LzxYd3raAt')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (2, N'FXBEUoebSzYYqlRiffOL')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (3, N'CNIsuq4hW93MWLzIf0yI')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (4, N'cOLDygzruqFkPmSAkLmI')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (5, N'OJNLuLK389BrdcWNXAVO')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (6, N'UtypXEdpUC0FyRPBZiLz')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (7, N'jRBFgC0blICgDGAIJkAA')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (8, N'4Hw7y2zRcJUUwIsm9ZdR')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (9, N'ACYIU1MJXhy2WaMvYDlS')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (10, N'd0v8ytZydWwLB3VbRyjS')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (11, N'FeTHYmUN6etjrAUmBSWb')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (12, N'VswDFWh5vMd8t9G45R24')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (13, N'G9U22ACSx7Gl7q0FXwjA')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (14, N'8ZnsbzfvISeECGQ2KKuO')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (15, N'OwaSX6VVScQDo8gF9v2M')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (16, N'gjzFRbaUc1npTrA6cQph')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (17, N'dpkufXUZoJAP9hddBfT6')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (18, N'hEcVKHzNXEBuStUB0r67')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (19, N'miSZ4wJZMaYt8Ox2mSVT')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (20, N'2QJXPBcpzFTs3fTYKqvq')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (21, N'Shi3HLAsavGq8eDkt2H6')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (22, N'Srm6uKxCCngsK03JKb1E')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (23, N'JTSMzBJy2SmQKOi5rHlD')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (24, N'dWoz2aC49kIUIfyC8f5N')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (25, N'1JlNF1O4M7IM0Bhs51aW')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (26, N'KiCoyyMSsy0UlEuQtZQe')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (27, N'RLekuwEr95eFgtO0bVGB')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (28, N'jbmwEAtbXu3yawFHGsnC')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (29, N'60MlfEvOvZ4rhEU1e9bm')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (30, N'UE0X9TD5bZV1p4dENkFx')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (31, N'EfKG3PBvewWI7a9UmatV')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (32, N'lybfNz04H36hqQFqHdtI')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (33, N'UNFyJj4VH7BOfmaXJRwO')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (34, N'enHw2DHFyKPRmknv0ig0')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (35, N'Wr4kq7bZoriAgI6HMRpM')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (36, N'0wrNjcK64sdXZO5roFrd')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (37, N'1ZVCk0L1UhF54AqnFLdW')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (38, N'ALtBGAdnQBE43Xvce2ky')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (39, N'rnauMNdgb1Em0NQAlEbW')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (40, N'1QANl2fsoYBRlbVG50zd')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (41, N'GTbDLu2u8jIdmjFwQma7')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (42, N'qIfj4h3Lx0EvUEfLIcUi')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (43, N'ihVPdpCCPh7WJcZhAGQA')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (44, N'a8Sk6jgi0ygoFjHZFtZr')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (45, N'idflqeIZYx6XKUBiUuzC')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (46, N'YkitRqQzWrKvi0im23jF')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (47, N'CgxNtREVciujM3omVP0K')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (48, N'vSDXy8O08IDY8uD8jrLe')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (49, N'in8eBM4aFwcwrWfFgEUh')
INSERT [dbo].[StaffPhoto] ([Id], [PhotoLink]) VALUES (50, N'lhcR7eFgMLJqFzZpREGh')
SET IDENTITY_INSERT [dbo].[StaffPhoto] OFF
GO
SET IDENTITY_INSERT [dbo].[StaffPosition] ON 

INSERT [dbo].[StaffPosition] ([Id], [Title], [Salary]) VALUES (1, N'Директор', CAST(110000.00 AS Decimal(10, 2)))
INSERT [dbo].[StaffPosition] ([Id], [Title], [Salary]) VALUES (2, N'Менеджер', CAST(75000.00 AS Decimal(10, 2)))
INSERT [dbo].[StaffPosition] ([Id], [Title], [Salary]) VALUES (3, N'Администратор', CAST(55000.00 AS Decimal(10, 2)))
INSERT [dbo].[StaffPosition] ([Id], [Title], [Salary]) VALUES (4, N'Помощник по залу', CAST(43500.00 AS Decimal(10, 2)))
INSERT [dbo].[StaffPosition] ([Id], [Title], [Salary]) VALUES (5, N'Уборщик', CAST(28000.00 AS Decimal(10, 2)))
SET IDENTITY_INSERT [dbo].[StaffPosition] OFF
GO
SET IDENTITY_INSERT [dbo].[Supplier] ON 

INSERT [dbo].[Supplier] ([Id], [Title], [RepresentetiveName], [Description], [Address], [Phone], [CityId], [PaymentAccount], [ITN]) VALUES (1, N'Компания №1', N'Агеева Е.М.', N'', N'ул. Неважно, д. 1', N'7(09)573-60-28', 4, N'79467496248 ', N'107382534434')
INSERT [dbo].[Supplier] ([Id], [Title], [RepresentetiveName], [Description], [Address], [Phone], [CityId], [PaymentAccount], [ITN]) VALUES (2, N'Компания №2', N'ТрофимовК.М.', N'', N'ул. Неважно, д. 2', N'7(39)323-63-12', 2, N'59336878099 ', N'107805370659')
INSERT [dbo].[Supplier] ([Id], [Title], [RepresentetiveName], [Description], [Address], [Phone], [CityId], [PaymentAccount], [ITN]) VALUES (3, N'Компания №3', N'КондрашоваВ.Н.', N'', N'ул. Неважно, д. 3', N'7(04)786-56-85', 1, N'38425140240 ', N'107270057594')
INSERT [dbo].[Supplier] ([Id], [Title], [RepresentetiveName], [Description], [Address], [Phone], [CityId], [PaymentAccount], [ITN]) VALUES (4, N'Компания №4', N'КонстантиноваС.К.', N'', N'ул. Неважно, д. 4', N'7(913)845-65-85', 5, N'18277337715 ', N'107796368542')
INSERT [dbo].[Supplier] ([Id], [Title], [RepresentetiveName], [Description], [Address], [Phone], [CityId], [PaymentAccount], [ITN]) VALUES (5, N'Компания №5', N'БогдановД.В.', N'', N'ул. Неважно, д. 5', N'7(288)290-30-54', 5, N'93033130204 ', N'107272196400')
INSERT [dbo].[Supplier] ([Id], [Title], [RepresentetiveName], [Description], [Address], [Phone], [CityId], [PaymentAccount], [ITN]) VALUES (6, N'Компания №6', N'ГончароваА.В.', N'', N'ул. Неважно, д. 6', N'7(4020)858-98-27', 3, N'42262367771 ', N'107920105590')
INSERT [dbo].[Supplier] ([Id], [Title], [RepresentetiveName], [Description], [Address], [Phone], [CityId], [PaymentAccount], [ITN]) VALUES (7, N'Компания №7', N'ДавыдовА.К.', N'', N'ул. Неважно, д. 7', N'7(74)603-98-17', 1, N'76024055990 ', N'107494854819')
INSERT [dbo].[Supplier] ([Id], [Title], [RepresentetiveName], [Description], [Address], [Phone], [CityId], [PaymentAccount], [ITN]) VALUES (8, N'Компания №8', N'ИсаеваП.П.', N'', N'ул. Неважно, д. 8', N'7(18)148-98-92', 3, N'82353555691 ', N'107541120696')
INSERT [dbo].[Supplier] ([Id], [Title], [RepresentetiveName], [Description], [Address], [Phone], [CityId], [PaymentAccount], [ITN]) VALUES (9, N'Компания №9', N'МолчановаТ.А.', N'', N'ул. Неважно, д. 9', N'7(8047)398-66-25', 4, N'59417394172 ', N'107860250991')
INSERT [dbo].[Supplier] ([Id], [Title], [RepresentetiveName], [Description], [Address], [Phone], [CityId], [PaymentAccount], [ITN]) VALUES (10, N'Компания №10', N'ЕжовД.М.', N'', N'ул. Неважно, д. 10', N'7(382)002-19-73', 1, N'11460208228 ', N'107493814677')
SET IDENTITY_INSERT [dbo].[Supplier] OFF
GO
SET IDENTITY_INSERT [dbo].[SupplyOrder] ON 

INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (1, N'#11489              ', 10, 57, CAST(31.00 AS Decimal(8, 2)), CAST(N'2016-04-26T23:34:00.000' AS DateTime), N'Коршунов Л. Е.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (2, N'#16354              ', 5, 56, CAST(183.00 AS Decimal(8, 2)), CAST(N'2006-06-17T04:25:00.000' AS DateTime), N'Одинцов З. Д.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (3, N'#13046              ', 9, 14, CAST(31.00 AS Decimal(8, 2)), CAST(N'2022-08-03T19:55:00.000' AS DateTime), N'Киселев М. Ф.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (4, N'#19127              ', 5, 14, CAST(164.00 AS Decimal(8, 2)), CAST(N'2013-08-10T20:13:00.000' AS DateTime), N'Белоусов А. Р.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (5, N'#19548              ', 7, 29, CAST(24.00 AS Decimal(8, 2)), CAST(N'2015-05-06T01:48:00.000' AS DateTime), N'Соболева Е. И.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (6, N'#15896              ', 6, 29, CAST(83.00 AS Decimal(8, 2)), CAST(N'2007-04-09T11:45:00.000' AS DateTime), N'Бабушкина А. Д.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (7, N'#14967              ', 10, 14, CAST(155.00 AS Decimal(8, 2)), CAST(N'2005-08-16T20:32:00.000' AS DateTime), N'Софронова Д. Т.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (8, N'#15285              ', 8, 24, CAST(193.00 AS Decimal(8, 2)), CAST(N'2010-04-17T04:11:00.000' AS DateTime), N'Попов А. М.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (9, N'#17598              ', 10, 20, CAST(42.00 AS Decimal(8, 2)), CAST(N'2010-12-28T08:51:00.000' AS DateTime), N'Коновалов А. А.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (10, N'#12175              ', 8, 38, CAST(136.00 AS Decimal(8, 2)), CAST(N'2004-09-17T13:36:00.000' AS DateTime), N'Рыбаков А. М.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (11, N'#10778              ', 7, 13, CAST(68.00 AS Decimal(8, 2)), CAST(N'2018-04-17T23:40:00.000' AS DateTime), N'Дегтярев Г. М.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (12, N'#13725              ', 10, 54, CAST(168.00 AS Decimal(8, 2)), CAST(N'2014-11-09T19:30:00.000' AS DateTime), N'Александров Г. Р.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (13, N'#15005              ', 5, 57, CAST(25.00 AS Decimal(8, 2)), CAST(N'2004-08-03T03:16:00.000' AS DateTime), N'Петрова Е. Р.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (14, N'#10771              ', 9, 33, CAST(42.00 AS Decimal(8, 2)), CAST(N'2011-03-25T23:19:00.000' AS DateTime), N'Литвинов А. М.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (15, N'#16938              ', 5, 64, CAST(125.00 AS Decimal(8, 2)), CAST(N'2006-02-22T08:43:00.000' AS DateTime), N'Иванова А. Ф.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (16, N'#16892              ', 8, 2, CAST(114.00 AS Decimal(8, 2)), CAST(N'2011-02-02T10:32:00.000' AS DateTime), N'Дмитриева П. А.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (17, N'#19227              ', 9, 28, CAST(137.00 AS Decimal(8, 2)), CAST(N'2008-10-05T12:38:00.000' AS DateTime), N'Чернова А. А.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (18, N'#10449              ', 8, 20, CAST(36.00 AS Decimal(8, 2)), CAST(N'2010-04-13T15:34:00.000' AS DateTime), N'Филиппов М. И.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (19, N'#11160              ', 10, 55, CAST(119.00 AS Decimal(8, 2)), CAST(N'2011-11-27T00:30:00.000' AS DateTime), N'Степанова А. П.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (20, N'#13359              ', 10, 45, CAST(69.00 AS Decimal(8, 2)), CAST(N'2017-06-21T16:45:00.000' AS DateTime), N'Горбунов М. Д.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (21, N'#17601              ', 3, 43, CAST(146.00 AS Decimal(8, 2)), CAST(N'2022-09-23T13:51:00.000' AS DateTime), N'Демидов Р. Г.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (22, N'#15518              ', 2, 49, CAST(53.00 AS Decimal(8, 2)), CAST(N'2022-02-16T06:31:00.000' AS DateTime), N'Платонов Д. А.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (23, N'#14769              ', 1, 65, CAST(91.00 AS Decimal(8, 2)), CAST(N'2013-09-15T08:23:00.000' AS DateTime), N'Андреев М. К.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (24, N'#16750              ', 9, 9, CAST(50.00 AS Decimal(8, 2)), CAST(N'2013-04-10T22:42:00.000' AS DateTime), N'Тихомиров Д. А.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (25, N'#15482              ', 7, 15, CAST(150.00 AS Decimal(8, 2)), CAST(N'2008-12-14T02:17:00.000' AS DateTime), N'Мартынов Д. Е.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (26, N'#12518              ', 2, 53, CAST(165.00 AS Decimal(8, 2)), CAST(N'2023-05-25T22:46:00.000' AS DateTime), N'Егорова С. А.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (27, N'#15167              ', 2, 35, CAST(92.00 AS Decimal(8, 2)), CAST(N'2018-01-15T03:33:00.000' AS DateTime), N'Трифонов Я. А.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (28, N'#15211              ', 8, 46, CAST(183.00 AS Decimal(8, 2)), CAST(N'2004-08-02T07:45:00.000' AS DateTime), N'Майорова К. М.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (29, N'#10388              ', 9, 13, CAST(96.00 AS Decimal(8, 2)), CAST(N'2018-09-03T08:28:00.000' AS DateTime), N'Воронина А. А.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (30, N'#13194              ', 2, 57, CAST(24.00 AS Decimal(8, 2)), CAST(N'2006-09-15T07:04:00.000' AS DateTime), N'Королев Д. В.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (31, N'#11536              ', 7, 20, CAST(46.00 AS Decimal(8, 2)), CAST(N'2004-01-28T11:21:00.000' AS DateTime), N'Болдырева А. Г.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (32, N'#12861              ', 4, 41, CAST(155.00 AS Decimal(8, 2)), CAST(N'2005-05-03T00:50:00.000' AS DateTime), N'Петровская А. П.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (33, N'#16777              ', 10, 46, CAST(174.00 AS Decimal(8, 2)), CAST(N'2009-01-06T09:24:00.000' AS DateTime), N'Белкин А. Д.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (34, N'#11843              ', 10, 63, CAST(189.00 AS Decimal(8, 2)), CAST(N'2006-09-19T14:15:00.000' AS DateTime), N'Павлова А. Л.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (35, N'#15561              ', 2, 12, CAST(186.00 AS Decimal(8, 2)), CAST(N'2009-06-24T13:16:00.000' AS DateTime), N'Елисеев Т. Г.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (36, N'#18629              ', 4, 46, CAST(193.00 AS Decimal(8, 2)), CAST(N'2011-03-06T11:54:00.000' AS DateTime), N'Лапин Б. В.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (37, N'#16785              ', 1, 54, CAST(107.00 AS Decimal(8, 2)), CAST(N'2008-02-23T10:29:00.000' AS DateTime), N'Калашникова В. Е.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (38, N'#17175              ', 10, 35, CAST(176.00 AS Decimal(8, 2)), CAST(N'2019-09-17T16:08:00.000' AS DateTime), N'Сухарева Е. Д.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (39, N'#19784              ', 4, 43, CAST(68.00 AS Decimal(8, 2)), CAST(N'2018-09-28T08:02:00.000' AS DateTime), N'Бородина В. З.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (40, N'#13002              ', 1, 30, CAST(119.00 AS Decimal(8, 2)), CAST(N'2017-04-15T01:56:00.000' AS DateTime), N'Максимова В. К.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (41, N'#10190              ', 9, 60, CAST(192.00 AS Decimal(8, 2)), CAST(N'2018-01-14T01:17:00.000' AS DateTime), N'Павлов К. Л.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (42, N'#19068              ', 2, 67, CAST(184.00 AS Decimal(8, 2)), CAST(N'2012-12-10T09:41:00.000' AS DateTime), N'Кириллова Т. Д.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (43, N'#13694              ', 8, 34, CAST(149.00 AS Decimal(8, 2)), CAST(N'2005-02-25T09:20:00.000' AS DateTime), N'Андреев А. С.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (44, N'#15413              ', 1, 14, CAST(189.00 AS Decimal(8, 2)), CAST(N'2020-08-23T22:13:00.000' AS DateTime), N'Овсянников Я. Т.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (45, N'#17107              ', 1, 45, CAST(81.00 AS Decimal(8, 2)), CAST(N'2013-08-08T02:11:00.000' AS DateTime), N'Зайцев Р. О.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (46, N'#15001              ', 6, 45, CAST(41.00 AS Decimal(8, 2)), CAST(N'2012-07-10T07:35:00.000' AS DateTime), N'Баранов Я. Т.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (47, N'#19878              ', 5, 59, CAST(77.00 AS Decimal(8, 2)), CAST(N'2023-04-07T04:52:00.000' AS DateTime), N'Губанов М. Т.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (48, N'#15009              ', 5, 42, CAST(109.00 AS Decimal(8, 2)), CAST(N'2014-08-24T22:46:00.000' AS DateTime), N'Аксенова С. Н.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (49, N'#13309              ', 6, 11, CAST(24.00 AS Decimal(8, 2)), CAST(N'2018-04-28T22:46:00.000' AS DateTime), N'Самойлова А. Р.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (50, N'#17086              ', 6, 34, CAST(17.00 AS Decimal(8, 2)), CAST(N'2007-09-02T06:30:00.000' AS DateTime), N'Морозова А. М.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (51, N'#14388              ', 4, 24, CAST(199.00 AS Decimal(8, 2)), CAST(N'2016-01-29T11:48:00.000' AS DateTime), N'Шарова Э. Д.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (52, N'#18745              ', 1, 57, CAST(148.00 AS Decimal(8, 2)), CAST(N'2017-10-12T03:44:00.000' AS DateTime), N'Николаев А. Д.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (53, N'#12102              ', 4, 5, CAST(188.00 AS Decimal(8, 2)), CAST(N'2004-05-14T10:34:00.000' AS DateTime), N'Воронин А. И.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (54, N'#11694              ', 7, 29, CAST(39.00 AS Decimal(8, 2)), CAST(N'2013-12-04T19:46:00.000' AS DateTime), N'Фомин Д. М.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (55, N'#10984              ', 1, 38, CAST(82.00 AS Decimal(8, 2)), CAST(N'2021-05-11T23:10:00.000' AS DateTime), N'Лопатин Е. П.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (56, N'#14062              ', 7, 30, CAST(133.00 AS Decimal(8, 2)), CAST(N'2020-05-14T16:45:00.000' AS DateTime), N'Колесникова Е. Р.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (57, N'#17072              ', 4, 23, CAST(36.00 AS Decimal(8, 2)), CAST(N'2004-02-14T19:07:00.000' AS DateTime), N'Иванов Л. П.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (58, N'#19063              ', 4, 27, CAST(27.00 AS Decimal(8, 2)), CAST(N'2011-09-03T19:50:00.000' AS DateTime), N'Мартынов М. М.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (59, N'#14560              ', 9, 3, CAST(72.00 AS Decimal(8, 2)), CAST(N'2006-09-05T09:32:00.000' AS DateTime), N'Зубов Т. А.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (60, N'#14431              ', 6, 17, CAST(6.00 AS Decimal(8, 2)), CAST(N'2019-07-11T14:01:00.000' AS DateTime), N'Давыдов К. М.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (61, N'#14903              ', 2, 63, CAST(119.00 AS Decimal(8, 2)), CAST(N'2005-08-18T17:01:00.000' AS DateTime), N'Киселева Т. Я.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (62, N'#12926              ', 8, 23, CAST(29.00 AS Decimal(8, 2)), CAST(N'2023-01-30T23:20:00.000' AS DateTime), N'Любимова Т. Т.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (63, N'#17213              ', 9, 44, CAST(188.00 AS Decimal(8, 2)), CAST(N'2023-03-23T03:26:00.000' AS DateTime), N'Миронов М. В.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (64, N'#13945              ', 1, 64, CAST(88.00 AS Decimal(8, 2)), CAST(N'2008-03-08T06:31:00.000' AS DateTime), N'Ефимова М. Н.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (65, N'#12079              ', 2, 51, CAST(61.00 AS Decimal(8, 2)), CAST(N'2022-02-12T03:55:00.000' AS DateTime), N'Демидова М. А.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (66, N'#10534              ', 4, 14, CAST(11.00 AS Decimal(8, 2)), CAST(N'2021-09-20T17:39:00.000' AS DateTime), N'Носова Е. Д.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (67, N'#14410              ', 1, 4, CAST(94.00 AS Decimal(8, 2)), CAST(N'2022-02-03T15:36:00.000' AS DateTime), N'Павлова К. М.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (68, N'#11376              ', 2, 54, CAST(80.00 AS Decimal(8, 2)), CAST(N'2005-04-09T09:09:00.000' AS DateTime), N'Мартынов Е. Н.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (69, N'#12973              ', 8, 22, CAST(25.00 AS Decimal(8, 2)), CAST(N'2008-10-19T19:12:00.000' AS DateTime), N'Королева М. А.')
INSERT [dbo].[SupplyOrder] ([Id], [OrderNum], [SupplierId], [ProductId], [ProductQuantity], [OrderDate], [CourierName]) VALUES (70, N'#18401              ', 8, 29, CAST(135.00 AS Decimal(8, 2)), CAST(N'2007-02-10T10:34:00.000' AS DateTime), N'Соколова Л. В.')
SET IDENTITY_INSERT [dbo].[SupplyOrder] OFF
GO
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [DF_Product_Description]  DEFAULT (NULL) FOR [Description]
GO
ALTER TABLE [dbo].[Sale] ADD  DEFAULT ((0)) FOR [Discount]
GO
ALTER TABLE [dbo].[Staff] ADD  DEFAULT (NULL) FOR [Patromimyc]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD FOREIGN KEY([MeasureUnitId])
REFERENCES [dbo].[MeasureUnit] ([Id])
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_ProductCategory] FOREIGN KEY([ProductCategoryId])
REFERENCES [dbo].[ProductCategory] ([Id])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_ProductCategory]
GO
ALTER TABLE [dbo].[ProductSale]  WITH CHECK ADD FOREIGN KEY([SaleId])
REFERENCES [dbo].[Sale] ([Id])
GO
ALTER TABLE [dbo].[ProductSale]  WITH CHECK ADD  CONSTRAINT [FK_ProductSale_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[ProductSale] CHECK CONSTRAINT [FK_ProductSale_Product]
GO
ALTER TABLE [dbo].[ProductSubCategory]  WITH CHECK ADD FOREIGN KEY([CategoryId])
REFERENCES [dbo].[ProductCategory] ([Id])
GO
ALTER TABLE [dbo].[ProductSubCategory]  WITH CHECK ADD FOREIGN KEY([SubCategoryId])
REFERENCES [dbo].[ProductCategory] ([Id])
GO
ALTER TABLE [dbo].[Sale]  WITH CHECK ADD FOREIGN KEY([DeliveryId])
REFERENCES [dbo].[DeliveryType] ([Id])
GO
ALTER TABLE [dbo].[Sale]  WITH CHECK ADD  CONSTRAINT [FK_Sale_Staff] FOREIGN KEY([StaffId])
REFERENCES [dbo].[Staff] ([Id])
GO
ALTER TABLE [dbo].[Sale] CHECK CONSTRAINT [FK_Sale_Staff]
GO
ALTER TABLE [dbo].[Staff]  WITH CHECK ADD FOREIGN KEY([PhotoId])
REFERENCES [dbo].[StaffPhoto] ([Id])
GO
ALTER TABLE [dbo].[Staff]  WITH CHECK ADD FOREIGN KEY([PositionId])
REFERENCES [dbo].[StaffPosition] ([Id])
GO
ALTER TABLE [dbo].[Supplier]  WITH CHECK ADD FOREIGN KEY([CityId])
REFERENCES [dbo].[City] ([Id])
GO
ALTER TABLE [dbo].[SupplyOrder]  WITH CHECK ADD FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([Id])
GO
ALTER TABLE [dbo].[SupplyOrder]  WITH CHECK ADD  CONSTRAINT [FK_SupplyOrder_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[SupplyOrder] CHECK CONSTRAINT [FK_SupplyOrder_Product]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[14] 4[48] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Product"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ProductCategory"
            Begin Extent = 
               Top = 6
               Left = 265
               Bottom = 102
               Right = 439
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MeasureUnit"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 234
               Right = 212
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_ProductList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_ProductList'
GO
USE [master]
GO
ALTER DATABASE [MaterialShop] SET  READ_WRITE 
GO
