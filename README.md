# 3ИСП11-20 Казаков Д. Магазин "Стройматериалы"

<b>[Ссылка на технические документы](https://gitlab.com/dimitandco/TEST_MDK)</b>

# Структура проекта

![structure](./resPhoto/AppStructure.png)

# Макет экрана авторизациии

![authWindow](./resPhoto/AuthScreen.png)

# Макет экрана списка товаров

![productWindow](./resPhoto/ProductScreen.png)

# Макет экрана создания заказа (корзина)

![newOrderWindow](./resPhoto/NewOrderScreen.png)

# Главное меню

![newOrderWindow](./resPhoto/MainScreen.png)

# Окно ввода промокода

![newOrderWindow](./resPhoto/CouponWindowScreen.png)
